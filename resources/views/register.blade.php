<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="fnama" id=""><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lnama" id=""><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" id="" value="Male">Male<br>
        <input type="radio" name="gender" id="" value="Female">Female<br>
        <input type="radio" name="gender" id="" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="national" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapura">Singapura</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Lainnya">Lainnya</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" id="">Bahasa Indonesia<br>
        <input type="checkbox" name="english" id="">English<br>
        <input type="checkbox" name="other" id="">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
