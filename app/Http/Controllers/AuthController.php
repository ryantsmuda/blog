<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('register');
    }

    public function submit(Request $request)
    {
        $fname = $request['fnama'];
        $lname = $request['lnama'];
        return view ('welcome', compact('fname', 'lname'));
    }
}
